App.AddUserPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    title: "Page",
    showTypeBox:true,
    parentId: 0,
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.title){

            this.title = options.title;
        }
        this.render();
    },
    events: {

        'click #closeBtn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render addGuestPopup");
        $.ajax({
         url : "php/html/user/addUserPopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    $("#create_box_title").text("Create new " + self.title);


                    $("#createBtn").on('click',function(){

                        if($("#firstNameInput").val() == "" || $("#lastNameInput").val().trim() == ""
                            || $("#emailInput").val().trim() == ""
                            || $("#passwordInput").val().trim() == ""
                            || $("#confirmPasswordInput").val().trim() == ""){
                            alert("All the fields are required.");
                            return;
                        }
                        else if($("#passwordInput").val() != $("#confirmPasswordInput").val()){
                            alert("Two password field are not matched.");
                            return;
                        }

                        $.ajax({
                            url : "api/user/updateUser.php",
                            method : "POST",
                            dataType: "json",
                            data : {
                                new:true,
                                firstName: $('#firstNameInput').val(),
                                lastName: $('#lastNameInput').val(),
                                email: $('#emailInput').val(),
                                pass: md5($('#passwordInput').val()),
                                role: $('#roleInput').val()
                            }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){
                            console.log("json = " + json);

                            if(json.status == 502){
                                alert("Session is time-out, please login again.");
                                location.reload();
                                return;
                            }

                            if(json.status == 501){
                                alert(json.msg);
                                return;
                            }
                            //close the popup
                            self.destroy();

                            App.userList.destroy();
                            App.userList.initialize();

                        }).error(function(d){
                            console.log('error')
                            console.log(d)
                        });
                    });

                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        $(".popup_box_container").remove();
        this.undelegateEvents();
        //Backbone.View.prototype.remove.call(this);
        //Remove view from DOM
        //this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});