App.MediaView = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    title: "Library",
    page:0,
    itemPerPage:20,
    initialize: function(options){
        if(options && options.parentId){
            this.parentId = options.parentId;
        }
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },

    render: function(){
        //alert($(window).width());
        var self = this;
        $.ajax({
            url : "php/html/mediaView.php",
            method : "POST",
            dataType: "html",
            data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(html){

            $(self.el).append(html).
                promise()
                .done(function(){
                    self.loadPhoto(self.page);
                });


            $.ajax({
                url : "api/getPhoto.php",
                method : "POST",
                dataType: "json",
                data : {getCount: true}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
            }).success(function(json){
                console.log("json = " + json);

                if(json.status == 502){
                    alert(App.strings.sessionTimeOut);
                    location.reload();
                    return;
                }
                console.log("json = " + json.data[0].totalMediaNum);
                console.log("json = " + json.data);

                $("#paginationContainer").pagination({
                    items: json.data[0].totalMediaNum,
                    itemsOnPage: 20,
                    cssStyle: 'light-theme',
                    onPageClick:function(pageNum, event){
                        //alert("hi" + pageNum);
                        self.page = pageNum -1;
                        self.loadPhoto(pageNum-1);
                        //$("#paginationContainer").pagination('selectPage', pageNum);
                    }
                });

            }).error(function(d){
                console.log('error');
                console.log(d);
            });


            $("#addBtn").on("click", function(){
                App.addPhotoPopup = new App.AddPhotoPopup(
                    {
                        root:self,
                        onlyShowUpLoad:true
                    }
                );
            });


        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    goBackToFirstPage: function(){
        $("#paginationContainer").pagination('selectPage', 1);
    },
    loadPhoto: function(page){
        $("#photoStreamContainer").empty();

        var self = this;
        $.ajax({
            url : "api/getPhoto.php",
            method : "POST",
            dataType: "json",
            data : {page:page}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(json){
            console.log(json);
            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            var imageArray = json.data;

            console.log(imageArray);

            var container;
            //self.imageId = json.data.photoId;
            for(var x = 0;x<imageArray.length;x++){
                console.log(imageArray[x].image);
                var extension = imageArray[x].fileExt == "j"?"jpg":"png";
                var image =document.createElement('div');
                $(image).attr("order",x);
                console.log("image = " + imageArray[x].image);
                //image.src = "upload/"+imageArray[x].image+"_s.jpg";
                $(image).addClass("image fixHeight")

                try{
                    $(image).css({
                        "background-image": 'url('+"upload/"+imageArray[x].image+"_s."+extension+')'
                    });
                }
                catch(e){
                    console.log("error = " + e);
                    $(image).css({
                        "background-image": 'url('+"upload/"+imageArray[x].image+"_s"+extension+')'
                    });
                }

                container = $("#photoStreamContainer");

                container.append("<div id=" + "pop_image"+(x+page*20) + "></div>");

                $("#pop_image"+(x+page*20)).addClass("imageRoot");

                $("#pop_image"+(x+page*20)).attr("order",(x+page*20));

                $("#pop_image"+(x+page*20)).css({"position":"relative","float":"left"});

                $("#pop_image"+(x+page*20)).append($(image));

                $("#pop_image"+(x+page*20)).on("click",function(){
                    console.log("clicked");
                    var order = $(this).attr("order");
                    /*$.ajax({
                     url : "api/addPhotoForItem.php",
                     method : "POST",
                     dataType: "json",
                     data : {mediaId:self.imageObjArray[order].id, itemId:App.currentId}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                     }).success(function(json){
                     console.log(json);

                     self.loadPhoto(self.page);
                     self.root.getPhoto();

                     }).error(function(d){
                     console.log('error');
                     console.log(d);
                     });*/
                });

                $("#pop_image"+(x+page*20)).append("<a class='removeImage' order=" + x + "></a>");

                $("#pop_image"+(x+page*20)).on("mouseover", function(){
                    $(this).find(".removeImage").show();
                });

                $("#pop_image"+(x+page*20)).on("mouseout", function(){
                    $(this).find(".removeImage").hide();
                });
            };

            //handle remove Image when click on remove button
            $(".removeImage").on("click",function(){
                console.log("delete position " +  $(this).attr('order'));

                var order = $(this).attr('order');
                App.yesNoPopup = new App.YesNoPopup(
                    {
                        yesFunc:function()
                        {

                            $.ajax({
                                url : "api/deleteItem.php",
                                method : "POST",
                                dataType: "json",
                                data : {id: imageArray[order].id, type:'media'}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                            }).success(function(json){

                                console.log(json);
                                //self.yesFunc();
                                App.yesNoPopup.destroy();

                                self.updateTotalMediaNum();

                            }).error(function(d){
                                console.log('error');
                                console.log(d);
                            });
                        },
                        msg:"Are you sure to delete this photo?"
                    }
                );
            });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    updateTotalMediaNum:function(){
        var self = this;
        $.ajax({
            url : "api/getPhoto.php",
            method : "POST",
            dataType: "json",
            data : {getCount: true}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(json){
            console.log("json = " + json);
            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            console.log("json = " + json.data[0].totalMediaNum);
            console.log("json = " + json.data);

            $("#paginationContainer").pagination('updateItems', json.data[0].totalMediaNum);


            //fine tune the max page number
            var maxPage = 0;

            maxPage = json.data[0].totalMediaNum / self.itemPerPage;

            console.log("maxPage = " + maxPage);


            if(json.data[0].totalMediaNum % self.itemPerPage > 0){
                maxPage++;
            }

            console.log("maxPage = " + maxPage);
            console.log("self.page = " + self.page);

            if(self.page +1  > maxPage){

                self.page = maxPage - 1;
                console.log("changed page = " + self.page);

            }
            $("#paginationContainer").pagination('selectPage', self.page+1);

            //self.loadPhoto(self.page);

            /*$("#paginationContainer").pagination({
                items: json.data[0].totalMediaNum,
                itemsOnPage: 20,
                cssStyle: 'light-theme',
                onPageClick:function(pageNum, event){
                    //alert("hi" + pageNum);
                    self.page = pageNum -1;
                    self.loadPhoto(pageNum-1);
                    //$("#paginationContainer").pagination('selectPage', pageNum);
                }
            });*/

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function(){
        var that = this;
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: that.title == "Library"?"Page":that.title,
                parentId: that.parentId
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});