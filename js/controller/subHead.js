App.SubHead = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#subhead_container',
    highlight:"library",
    path_length:0,
    path:["fasdf","fasdf"],
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.highlight){
            this.highlight = options.highlight;

            //alert(this.highlight);
        }
        if(options && options.paths){

            this.path = options.paths;
        }

        this.render();
    },
    itemOption: {
        "content": {
            "name": ["Services","Dining", "Concierge", "Photo", "Keys"],
            "list": ["content/library","content/dining","content/concierge", "content/photo", "content/keys"]
        },
        "setup": {
            "name": ["Airline", "Airport"],
            "list": ["setup/airline", "setup/airport"]
        },
        "message": {
            "name": [],
            "list": []
        }
    }
    ,
    events: {
        'click #cameraBtn': 'goToCamera',
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        var self = this;
        //alert($(window).width());
        console.log("render Subhead");

        if(this.highlight == "pick") {
            this.highlight = this.getRootCategory();
        }

        //this call is for store all title and types of all item.
        $.ajax({
            url : "api/getItemList.php",
            method : "POST",
            dataType: "json",
            data : {getAll:1, light:1 }
        }).success(function(json){
            //alert(json);
            console.log("ItemLight = " + json);
            for(var y = 0; y < json.data.length;y++){
                //console.log("fucker");
                //we store the id/title key/value pair using the singleton App object
                if(App.idArray[json.data[y].id] == null){
                    App.idArray[json.data[y].id] = json.data[y].title_en;
                    //console.log("inport");
                }

                //we store the id/type key/value pair using the singleton App object
                if(App.typeArray[json.data[y].id] == null){
                    App.typeArray[json.data[y].id] = json.data[y].type;
                    console.log("inport2");
                }
                else{
                    console.log("tp = " + App.typeArray[json.data[y].id]);
                }

            }
            console.log("app idArray = " + App.idArray);

            console.log("1 App typeArray = " + App.typeArray);
            console.log("1 App typeArray = " + App.typeArray.length);
            self.initUI();

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });


    },
    initUI : function(){
        console.log("initUi fire");
        var self = this;

        $(this.el).append("<div class='pages-route'> </div>");
        var link = "";

        var foundRootType = null;

        self.path_length = self.path.length;

        console.log("self.path_length  = " + self.path_length );


        //generate the link path  e.g, home -> section -> item
        for(var x = 0; x < self.path.length; x++){

            if(self.path[x] == "property"){
                link = "#/property/";
            }
            else{
                link+=self.path[x]+"/";
            }

            if(self.path[x] == App.hotelLocation){
                continue;
            }

            //if this is not the last item, we give it a link
            if(x != self.path.length-1){
                $(".pages-route").append("<a id='a"+x+ "'/>");

                //block the property temporarily
                if(self.path[x] != "property"){
                    $("#a"+x).attr('href',link);
                }
                $(".pages-route").append("<span class='breadcrumbs-separator'>&nbsp;></span>");
            }
            else{
                $(".pages-route").append("<div id='a"+x+ "'/>");
            }

            if(self.path[x].length  > 35){
                $("#a"+x).text(App.idArray[self.path[x]]);
                console.log("A");

                if(foundRootType == null){
                    if(App.typeArray[self.path[x]] == 'Information (with top sub menu)'){
                        foundRootType = true;
                        App.haveItem = true;
                        //alert("have item = true");
                    }
                    else{
                        foundRootType = false;
                        App.haveItem = false;
                        //alert("have item = false");
                    }
                }
            }
            else{
                $("#a"+x).text(self.path[x]);
                console.log("B");
            }

            console.log("for looping" + x );
        }

        var typeObject = null;
        var index = 0;
        if(this.highlight == 'library' || this.highlight == 'dining'|| this.highlight == 'concierge'|| this.highlight == 'photo' || this.highlight == 'keys'){
            typeObject = this.itemOption.content;

            switch(this.highlight){
                case "library" :
                    index = 0;
                    break;
                case "dining" :
                    index = 1;
                    break;
                case "concierge" :
                    index = 2;
                    break;
                case "photo":
                    index = 3;
                    break;
                case "keys":
                    index = 4 ;
                    break;
                default:
                    break;
            }
        }
        else if(this.highlight == 'active' || this.highlight == 'inactive'){
            typeObject = this.itemOption.message;

            switch(this.highlight){
                case "active" :
                    index = 0;
                    break;
                case "inactive":
                    index = 1;
                    break;
                default:
                    break;
            }
        }
        else if(this.highlight == 'airline' || this.highlight == 'airport' || this.highlight == 'FX Currency'){
            typeObject = this.itemOption.setup;

            switch(this.highlight){
                case "airline" :
                    index = 0;
                    break;
                case "airport":
                    index = 1;
                    break;
                case "FX Currency":
                    index = 2;
                    break;
                default:
                    break;
            }
        }

        $(this.el).append("<div class='sub-menu'>");

        for(var x = 0;x < typeObject.name.length;x++){
            $(".sub-menu").append("<div class='sub-menu-btn btn btn-block btn-default' id='"+x+"Btn'>"+typeObject.name[x]+"</div>");
        }

        $(this.el).append("</div>");

        $("#"+index+"Btn").removeClass("btn-default");
        $("#"+index+"Btn").addClass("bg-olive");

        $("#0Btn").on("click", function(){
            $(".sub-menu-btn").removeClass("bg-olive");
            $(".sub-menu-btn").removeClass("btn-default");
            $(".sub-menu-btn").addClass("btn-default");
            $(this).removeClass("btn-default");
            $(this).addClass("bg-olive");

            window.location = document.URL = window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/'+App.baseFolder+'/#/property/'+App.hotelLocation+'/'+typeObject.list[0];
        });
        $("#1Btn").on("click", function(){
            $(".sub-menu-btn").removeClass("bg-olive");
            $(".sub-menu-btn").removeClass("btn-default");
            $(".sub-menu-btn").addClass("btn-default");
            $(this).removeClass("btn-default");
            $(this).addClass("bg-olive");
            window.location = document.URL = window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/'+App.baseFolder+'/#/property/'+App.hotelLocation+'/'+typeObject.list[1];
        });
        $("#2Btn").on("click", function(){
            $(".sub-menu-btn").removeClass("bg-olive");
            $(".sub-menu-btn").removeClass("btn-default");
            $(".sub-menu-btn").addClass("btn-default");
            $(this).removeClass("btn-default");
            $(this).addClass("bg-olive");
            window.location = document.URL = window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/'+App.baseFolder+'/#/property/'+App.hotelLocation+'/'+typeObject.list[2];
        });
        $("#3Btn").on("click", function(){
            $(".sub-menu-btn").removeClass("bg-olive");
            $(".sub-menu-btn").removeClass("btn-default");
            $(".sub-menu-btn").addClass("btn-default");
            $(this).removeClass("btn-default");
            $(this).addClass("bg-olive");
            window.location = document.URL = window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/'+App.baseFolder+'/#/property/'+App.hotelLocation+'/'+typeObject.list[3];
        });
        $("#4Btn").on("click", function(){
            $(".sub-menu-btn").removeClass("bg-olive");
            $(".sub-menu-btn").removeClass("btn-default");
            $(".sub-menu-btn").addClass("btn-default");
            $(this).removeClass("btn-default");
            $(this).addClass("bg-olive");
            window.location = document.URL = window.location.protocol+'//'+window.location.hostname+':'+window.location.port+'/'+App.baseFolder+'/#/property/'+App.hotelLocation+'/'+typeObject.list[4];
        });

    },
    getParentText:function(){
        console.log("this.path_length =" + this.path_length);
        console.log("name=" + this.path[this.path_length-1]);
        return App.idArray[this.path[this.path_length-1]];
    },
    getParentType:function(){
        console.log("get Parent Type fire");

        var ret = {
            "type": "temp",
            "isChild": false
        };

        for(var x = 0; x < this.path.length; x++) {
            console.log("each path = " + this.path[x]);
            console.log("each path length = " + this.path[x].length);
            //this is the first object, which is the parent
            if (this.path[x].length > 35) {
                console.log("x = " +x);
                console.log("this.path.length-1 = " + (this.path.length-1)  );

                if(x == this.path.length-1){
                    ret.isChild = true;
                    if(this.path[x-1] == "library" || this.path[x-1] == "dining" || this.path[x-1] == "concierge"){
                        ret.isChild = false;
                    }
                    else{
                    }
                }


            }
            if(this.path[x] == "library" || this.path[x] == "dining" || this.path[x] == "concierge"){
                ret.type =  App.typeArray[this.path[x+1]];
            }
        }

        return ret;
    },

    getRootCategory:function(){
        console.log("getRootCategory fire");

        var ret = "library";

        for(var x = 0; x < this.path.length; x++) {
            console.log("each path = " + this.path[x]);
            console.log("each path length = " + this.path[x].length);
            //this is the first object, which is the parent
            if (this.path[x] == 'content') {

                ret = this.path[x+1];
                break;
            }

        }

        return ret;
    },

    setPathText: function(index,text){

        //alert("setPathText:"  + text + " ");
        if(index == -1){
            index = this.path_length-1;
        }
        //alert(index);
        $("#a"+index).text(text);
    },
    goToCamera: function(){
        $("#takePictureField").trigger("click");
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();

        //this.$el.removeData().unbind();

        //Remove view from DOM
        this.remove();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});