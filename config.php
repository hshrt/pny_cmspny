<?php
ini_set( "display_errors", true );
date_default_timezone_set( "Asia/Hong_Kong" );  // http://www.php.net/manual/en/timezones.php
define( "DB_DSN", "mysql:host=localhost;dbname=BSPPNY;charset=utf8" );
define( "DB_USERNAME", "marco" );
define( "DB_PASSWORD", "marco.esd");

define( "DB_DSN_BPC", "mysql:host=localhost;dbname=bpc;charset=utf8" );
define( "DB_USERNAME_BPC", "marco" );
define( "DB_PASSWORD_BPC", "marco.esd");

//Weather server login info
define( "DB_DSN_WEATHER", "mysql:host=phkhgc.hsh-grt.com;dbname=Weather;charset=utf8" );
define( "DB_USERNAME_WEATHER", "marco" );
define( "DB_PASSWORD_WEATHER", "marco");



define("BASE_FOLDER", "cmspny");
define("HOTEL_LOCATION", "newyork");
define("SERVER_TYPE", "production");
define("HEADER_TEXT", "HSH PNY CMS PRODUCTION");
define("VERSION_NUM", "1.1.2");

//only useful for data Migration call
define( "ROOT_PATH", "http://192.168.11.61/cms/" );


define("OK",200);
define("Created",201);
define("No_Content",204);
define("Not_Modified",304);
define("Bad_Request",400);
define("Unauthorized",401);
define("Forbidden",403);
define("Not_Found",404);
define("Method_Not_Allowed",405);
define("Gone",410);
define("Unsupported_Media_Type",415);
define("Unprocessable_Entity",422);
define("Too_Many_Requests",429);

define("Invalid_input",501);
define("Session_timeout",502);

function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}

set_exception_handler( 'handleException' );
?>
