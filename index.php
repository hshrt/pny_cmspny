<?php

require( "config.php" );?>

<?php session_start(); include "php/html/header.php"?>


<body class="skin-blue layout-top-nav">
<div class="callout" id="tipsBox">
    <h4 id="tipsTitle">I am a success callout!</h4>
    <p id="content">This is a green callout.</p>
</div>
<div id="container">

    <!--if user is logged in-->
    <?php if(isset($_SESSION['email'])){?>
    <div id="topBar">
        <img src="images/logo.png" style="margin:20px">


        <div id="logoutBtn">Log out</div>
        <div id="seperator"> | </div>
        <div id="mailContainer">
        <div id="userEmail"></div>
        <div id="changePW">change password</div>
        </div>


    </div>



    <div id='pages-container'>
        <div id='subhead_container'>
        </div>
        <div id='content_container' class="content-wrapper">
        </div>
    </div>

    <div id="loadContainer"></div>
    <?php }?>

    <script>
        $(function(){

            //load the config data from PHP
            App.baseFolder = "<?php echo BASE_FOLDER; ?>";
            App.hotelLocation = "<?php echo HOTEL_LOCATION; ?>";
            App.serverType = "<?php echo SERVER_TYPE; ?>";

            //if user is not logged in, show Login View
            <?php if(!isset($_SESSION['email'])){?>

                App.signInView = new App.SignInView();
            <?php }else {?>


            //load the user related data from the SESSION
            App.userEmail = "<?php echo($_SESSION['email'])?>";
            App.userRole = "<?php echo($_SESSION['role'])?>";
            App.userId = "<?php echo($_SESSION['userId'])?>";



           

            App.init();

            $.address.change(function(event) {
                // do something depending on the event.value property, e.g.
                // $('#content').load(event.value + '.xml');
                console.log(event);

                //everytime there is a action, check whether there the session timeout, if yes, go to login page
                <?php if(!isset($_SESSION['email'])){?>

                    location.reload();
                    return;
                <?php }?>
                console.log("<?php echo $_SESSION['email']?>");
                //initially hide the loading
                App.hideLoading();

                var param = $.address.pathNames();

                App.subTopBar.hightlightTab(param[2]);

                console.log(param);
                //alert("param count = "+ param.length);
                /*switch (param[param.length-1]){
                    case 'library':
                        $("#subhead_container").empty();
                        $("#content_container").empty();
                        App.subHead = new App.SubHead();
                        App.libList = new App.ItemList();
                        break;
                    case <100:
                    default:
                        break;
                }*/

                $("#subhead_container").empty();
                $("#content_container").empty();

                App.subHead = null;
                App.libList = null;
                App.mediaView = null;
                App.keysView = null;
                App.guestList = null;
                App.messageList = null;
                App.messageDetail = null;

                if(param[param.length-1] == 'guests'){

                    App.subHead = new App.SubHead({highlight:param[param.length-1],paths:param});
                    App.guestList = new App.GuestList();
                }
                else if(param[param.length-1] == 'airline' || param[param.length-1] == 'setup' ){
                    App.subHead = new App.SubHead({highlight:"airline",paths:param});
                    App.aviationList = new App.AviationList({type:"airline"});
                }
                else if(param[param.length-1] == 'airport' ){
                    App.subHead = new App.SubHead({highlight:param[param.length-1],paths:param});
                    App.aviationList = new App.AviationList({type:"airport"});
                }
                else if(param[param.length-1] == 'fx'){
                    App.subHead = new App.SubHead({highlight:"FX Currency",paths:param});
                    App.fxList = new App.FxList({type:"fx"});
                }
                else if(param[param.length-1] == 'messages'){

                    App.subHead = new App.SubHead({highlight:'active',paths:param});
                    App.messageList = new App.MessageList({type:'active'});
                }
                else if(param[param.length-1] == 'active' || param[param.length-1] == 'inactive'){

                    App.subHead =  new App.SubHead({highlight:param[param.length-1],paths:param});
                    App.messageList = new App.MessageList({type:param[param.length-1]});
                }
                else if(param[param.length-1] == 'new' && param[param.length-2] == 'active'){

                    App.subHead = new App.SubHead({highlight:param[param.length-1],paths:param});
                    App.messageDetail = new App.MessageDetail({type:"new"});
                }
                else if(param[param.length-1] == 'new' && param[param.length-2] == 'inactive'){

                    window.location = document.URL = window.location.protocol+'//'+window.location
                        .hostname+'/cms/#/property/'+App.hotelLocation+'/messages/active/new';
                }
                else if(param[param.length-1] == "user"){
                    App.userList = new App.UserList({type:'active'});
                }
                else if(param[param.length-1] == 'library' || param[param.length-1] == 'content'){

                    App.subHead = new App.SubHead({highlight:"library",paths:param});
                    App.libList = new App.ItemList();
                }
                else if(param[param.length-1] == 'dining'){

                    App.subHead = new App.SubHead({highlight:"dining",paths:param});
                    App.libList = new App.ItemList({parentId:1});
                }
                else if(param[param.length-1] == 'concierge'){

                    App.subHead = new App.SubHead({highlight:"concierge",paths:param});
                    App.libList = new App.ItemList({parentId:2});
                }
                else if(param[param.length-1] == 'photo'){
                    console.log("mediaView = " + App.mediaView);

                    if(App.mediaView == null){
                        App.subHead = new App.SubHead({highlight:"photo",paths:param});
                        App.mediaView = new App.MediaView();
                    }
                }
                else if(param[param.length-1] == 'keys'){
                    console.log("keys = " + App.keysView);

                    if(App.keysView == null){
                        App.subHead = new App.SubHead({highlight:"keys",paths:param});
                        App.keysView = new App.KeysView();
                    }
                }
                else if(param[param.length-1] == 'changePW'){
                    App.userPW = new App.UserPW( );
                }

                else if(param[param.length-1].length > 35){
                    //alert(param[param.length-1]);
                    App.currentId = param[param.length-1];

                    if(param[param.length-2] == 'guests'){
                        App.subHead = new App.SubHead({highlight:param[param.length-1],paths:param});
                        App.guestDetail = new App.GuestDetail();
                    }
                    else if(param[param.length-3] == 'messages' || param[param.length-2] == 'messages'){
                        App.subHead = new App.SubHead({highlight:param[param.length-1],paths:param});
                        App.messageDetail = new App.MessageDetail({});
                    }
                    else if(param[param.length-2] == 'user' ){

                        App.subHead = new App.SubHead({highlight:param[param.length-1],paths:param});
                        App.userDetail = new App.UserDetail({});
                    }

                    else{
                        App.subHead = new App.SubHead({highlight:"pick",paths:param});
                        App.itemDetail = new App.ItemDetail({paths:param});
                    }
                }
                else{
                    $("#content_container").append("Under Construction");
                }
            });

            <?php } ?>
        });
    </script>


<?php  if(isset($_SESSION['email'])){
        include "php/html/footer.php";
        }
?>