<?php 

require( "../config.php" );
require("../php/inc.appvars.php");

session_start();
//include("checkSession.php");

$price= isset($_POST["price"])?$_POST["price"]:null;
$item_id= isset($_POST["item_id"])?$_POST["item_id"]:null;
$command= isset($_POST["command"])?$_POST["command"]:null;
$availTime = isset($_POST["timeAvail"])?$_POST["timeAvail"]:null;
$shortDes = isset($_POST["shortDes"])?$_POST["shortDes"]:"";
$print = isset($_POST["print"])?$_POST["print"]:0;
$maxChoice = isset($_POST["maxChoice"])?$_POST["maxChoice"]:1;
$minChoice = isset($_POST["minChoice"])?$_POST["minChoice"]:1;
$maxQuantity = isset($_POST["maxQuantity"])?$_POST["maxQuantity"]:1;
$startTime = isset($_POST["startTime"])?$_POST["startTime"]:"00:00:00";
$endTime = isset($_POST["endTime"])?$_POST["endTime"]:"00:00:00";
$optionSetIds = isset($_POST["optionSetIds"])?$_POST["optionSetIds"]:"";
$complexOption = isset($_POST["complexOption"])?$_POST["complexOption"]:0;
//echo($command);
if(!is_numeric($price)){
    echo returnStatus(0 , 'The price must be number.',array('id' => $item_id));
    exit;
}


// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE items SET price=:price,command=:command, print=:print, lastUpdate=now(),
maxChoice=:maxChoice, minChoice=:minChoice, maxQuantity=:maxQuantity,  startTime=:startTime,  
endTime=:endTime, optionSetIds = :optionSetIds, complexOption=:complexOption,
lastUpdateBy=:email
where
id =
 :item_id";
$st = $conn->prepare ( $sql );

$st->bindValue( ":price", strval($price), PDO::PARAM_STR);
$st->bindValue( ":command", strval($command), PDO::PARAM_STR);
//$st->bindValue( ":shortDes", strval($shortDes), PDO::PARAM_STR);
$st->bindValue(":print", $print, PDO::PARAM_STR);
$st->bindValue( ":item_id", $item_id, PDO::PARAM_STR);
$st->bindValue( ":maxChoice", $maxChoice, PDO::PARAM_INT);
$st->bindValue( ":minChoice", $minChoice, PDO::PARAM_INT);
$st->bindValue( ":maxQuantity", $maxQuantity, PDO::PARAM_INT);
$st->bindValue( ":startTime", $startTime, PDO::PARAM_STR);
$st->bindValue( ":endTime", $endTime, PDO::PARAM_STR);
$st->bindValue( ":optionSetIds", $optionSetIds, PDO::PARAM_STR);
$st->bindValue( ":complexOption", $complexOption, PDO::PARAM_STR);
$st->bindValue( ":email", "system", PDO::PARAM_STR);
$st->execute();

$conn = null;

echo returnStatus(1 , 'good',array('id' => $item_id));
?>
