<?php
/**
 * Created by PhpStorm.
 * User: esd
 * Date: 9/22/14
 * Time: 10:12 AM
 */
ini_set( "display_errors", true );


/*require( "../config.php" );
require("../php/inc.appvars.php");
require("../php/func_nx.php");*/

//for production
require( "../config.php");
require("../php/inc.appvars.php");
require("../php/func_nx.php");


//delete message where room is checked out
//get all the rows from allroom
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "select * from allroom";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$room_to_delete_list = array();

for($x=0;$x<sizeof($list);$x++){
    $item = $list[$x];
    //echo PHP_EOL.$item["room"]." = ".strlen($item["guestname"]).PHP_EOL;

    if(strlen($item["guestname"])==0){
        array_push($room_to_delete_list,$item["room"]);
    }
}
echo PHP_EOL."START DELETE".PHP_EOL;
//array_push($room_to_delete_list,322);

pprint_r($room_to_delete_list);

for($y=0;$y<sizeof($room_to_delete_list);$y++){
    $sql = "select * from message m left join roomMessageMap rmm on m.id = rmm.messageId where rmm
    .room=$room_to_delete_list[$y];";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $msglist = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $msglist[] = $row;
    }
    for($z=0;$z<sizeof($msglist);$z++){
        echo 'checkout room'. PHP_EOL;
        $sql = "UPDATE message SET status='D', lastUpdate=now() where id = :id AND boardcast != 2";
        $st2 = $conn->prepare ( $sql );
        $st2->bindValue( ":id",  $msglist[$z]['id'], PDO::PARAM_STR );
        $st2->execute();
    }

    $sql = "DELETE FROM roomMessageMap where room = $room_to_delete_list[$y]";
    $st3 = $conn->prepare ( $sql );
    $st3->execute();

    $sql = "UPDATE orders SET deleted=1 where room = $room_to_delete_list[$y]";
    $st4 = $conn->prepare ( $sql );
    $st4->execute();
}

$conn = null;



?>