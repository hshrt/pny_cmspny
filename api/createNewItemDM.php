<?php 

require("../config.php");
require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();
  //include("checkSession.php");

$title = isset($_REQUEST['title'])?$_REQUEST['title']:null;
$type = isset($_REQUEST['type'])?$_REQUEST['type']:null;

$skipCreateDescription = 0;

if(isset($_REQUEST['skipCreateDescription'])){
    $skipCreateDescription = $_REQUEST['skipCreateDescription'];
}

$parentId = 0;

if(isset($_POST['parentId'])){
    $parentId = $_POST['parentId'];
}

if(empty($title) || empty($type)){
    echo returnStatus(Invalid_input , "All field cannot be empty.");
    exit;
}


// Does the Article object already have an ID?
//if ( !is_null( $this->id ) ) trigger_error ( "Article::insert(): Attempt to insert an Article object that already has its ID property set (to $this->id).", E_USER_ERROR );

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$uuid = $list[0]["UUID"];



$sql = "INSERT INTO dictionary (id,en,lastUpdate, lastUpdateBy) VALUES (:id,:title,now(),:email)";
$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $uuid, PDO::PARAM_STR );
$st->bindValue( ":title", $title, PDO::PARAM_STR );
$st->bindValue( ":email", "system", PDO::PARAM_STR );
$st->execute();
$titleId = $uuid;

$desId = 'descriptionIdTemp';

if(($type == "article" || $type == "item" || $type == "Restaurant" || $type=="sub_item") && $skipCreateDescription == 0){

    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid_des = $list[0]["UUID"];

    $sql = "INSERT INTO dictionary (id,en,lastUpdate, lastUpdateBy) VALUES (:id,:title,now(),:email)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid_des, PDO::PARAM_STR );
    $st->bindValue( ":title", "", PDO::PARAM_STR );
    $st->bindValue( ":email", "system", PDO::PARAM_STR );
    $st->execute();
    $desId = $uuid_des;
    //echo("desId = ".$desId);
}

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$uuid_obj = $list[0]["UUID"];

$sql = "INSERT INTO items (items.id,titleId, descriptionId, type, parentId,lastUpdate ,lastUpdateBy,items.order) VALUES (:uuid,
:titleId,:desId, :type, :parentId, CURRENT_TIMESTAMP,:email,1000000)";
$st = $conn->prepare ( $sql );

$st->bindValue( ":uuid", $uuid_obj, PDO::PARAM_STR );
$st->bindValue( ":titleId", $titleId, PDO::PARAM_STR );
$st->bindValue( ":desId", $desId, PDO::PARAM_STR );
$st->bindValue( ":type", $type, PDO::PARAM_STR );
$st->bindValue( ":parentId", $parentId, PDO::PARAM_STR );
$st->bindValue( ":email", "system", PDO::PARAM_STR );
$st->execute();
//$this->id = $conn->lastInsertId();
$conn = null;
//echo $sql;

//this code print the error of running sql, useful
//print_r($db->errorInfo());

//header( "Location: ../index.php" );

if($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, $titleId.",".$desId.",".$uuid_obj);
}
else{
    echo returnStatus(0, 'create item fail');
}
?>
