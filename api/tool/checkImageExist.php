<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */
/*this API is only for doing data migration from Itellity PHK DB to R&T DB*/

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$imageName = isset($_REQUEST['imageName'])?$_REQUEST['imageName']:'';
$fileHash = isset($_REQUEST['fileHash'])?$_REQUEST['fileHash']:'';

//$hash1 = md5_file('http://210.17.193.240:1337/files/images/782d3f94-0297-4160-9551-e0c9f9dabd1e.jpg');
//$hash2 = md5_file('/var/www/html/cmsphk/upload/'.'b4374cf5-d760-4f2b-aad2-c68d166d71a0_1488728986_16842.jpg');

/*pprint_r($hash1);
pprint_r($hash2);*/

//exit;

//checkifImageExist($fileHash);
//exit;

if ( empty($imageName)){
    echo returnStatus(0, 'missing image name');

    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "select * from media where fileName like :imageName";

$imageNameToCheck = checkifImageExist($fileHash);

//pprint_r('imageNameToCheck=');
//pprint_r($imageNameToCheck);

$st = $conn->prepare ( $sql );
$st->bindValue( ":imageName", $imageNameToCheck.'%', PDO::PARAM_STR );

$st->execute();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(0, 'there is existing image',$list);
}
else{
    echo returnStatus(1, 'There is no duplicate image, can create!');
}

return 0;

function file_list($d,$x){
    $l = array();
    foreach(array_diff(scandir($d),array('.','..')) as $f)if(is_file($d.'/'.$f)&&(($x)?ereg($x.'$',$f):1))$l[]=$d.$f;
    return $l;
}

//this method check if there is image with same content existed in the "upload" folder, this avoid same image uploaded for multiple time
function checkifImageExist($fileHash){

    $dir_array = array("/var/www/html/cmsphk/upload/");
    $imageFiles = array();

    foreach($dir_array as $dir){
        //pprint_r(file_list($dir,".rec"));
        $imageFiles = array_merge($imageFiles,file_list($dir,".jpg"));
    }

    $filterImageFiles = array();

    foreach($imageFiles as $imageFile){
        $len = strlen($imageFile);
        //pprint_r($imageFile);
        //pprint_r(substr($imageFile,$len-6,2));
        $substr = substr($imageFile,$len-6,2);
        if(!($substr=='_m' || $substr=='_s')){
            $imageFile = substr($imageFile, strpos($imageFile,"upload")+7, strlen($imageFile)-1);
            array_push($filterImageFiles,$imageFile);
        }

    }

    //pprint_r($filterImageFiles);

    foreach($filterImageFiles as $file){

        //pprint_r($file);
        //pprint_r(md5_file("/var/www/html/cmsphk/upload/".$file));
        if($fileHash==md5_file("/var/www/html/cmsphk/upload/".$file)){
            //pprint_r(substr($file,0,strlen($file)-1-4));
            return(substr($file,0,strlen($file)-1-4));
            //there is same image
            //break;
        }
    }

    return "fasdfklasdjflkjasdkfjlaksdfkasdkfasdfsadkj";
}

?>
