<?php
require( "../config.php" );

ini_set( "display_errors", true );

require("../php/inc.appvars.php");
require("../php/func_nx.php");

$parentId = null;

if(isset($_REQUEST["parentId"])){
    $parentId = $_REQUEST["parentId"];
    $parentId=$parentId.'';
}

$itemId = null;
if(isset($_REQUEST["itemId"])){
    $itemId = $_REQUEST["itemId"];
}

$getAll = null;
if(isset($_REQUEST["getAll"])){
$getAll = $_REQUEST["getAll"];
}

$light = null;
if(isset($_REQUEST["light"])){
    $light = $_REQUEST["light"];
}

$appVersion = isset($_REQUEST["appVersion"])?$_REQUEST["appVersion"]:null;
if($appVersion!=null){
    $appVerNumArr=explode('.', $appVersion);
}

$source = isset($_REQUEST["source"])?$_REQUEST["source"]:null;


$lang = isset($_REQUEST["lang"]) ?$_REQUEST["lang"]:"en";

//
$exclusionKey = '9168ebca-bdd6-11e6-9f07-984be11049af';

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sqlForSpecificId = '';

$sqlForNeedParentId = '';

if(isset($_POST["parentId"])){
    $sqlForNeedParentId = ' && items.parentId = :parentId ';
}

if($itemId != null){
    $sqlForSpecificId = ' && items.id = :id ';
}

$sql = "SELECT SQL_CALC_FOUND_ROWS items.*, UNIX_TIMESTAMP(items.lastUpdate) AS lastUpdate, d1.".$lang." AS title_en ,
d2.".$lang. " AS description_en, coalesce(d3.childNames, '') AS child_names, d25.fileName,d4.iconName,d4.ext, items
.enable
			FROM items
			INNER JOIN dictionary d1
			ON items.titleId = d1.id ".$sqlForNeedParentId.$sqlForSpecificId.
			"INNER JOIN dictionary d2
			ON items.descriptionId = d2.id

            LEFT JOIN (select fileName AS iconName, fileExt as ext, m2.itemId from media inner join mediaItemMap m2
            on media.id = m2.mediaId && m2.isIcon = 1) d4
            ON items.id = d4.itemId

            LEFT JOIN (select fileName, m1.itemId from media inner join mediaItemMap m1
            on media.id = m1.mediaId && m1.prefer = 1) d25
            ON items.id = d25.itemId

			LEFT JOIN
			(SELECT  GROUP_CONCAT(i1.en) AS childNames, items.parentId as parentId
			FROM items



			LEFT JOIN (SELECT  DISTINCT i2.parentId  as parentId, i2.id as id
								FROM items
								INNER JOIN  items i2
								ON items.id = i2.parentId) unqiueP
				ON items.parentId = unqiueP.parentId

			INNER JOIN  dictionary i1
			ON items.titleId = i1.id && items.id = unqiueP.id
			GROUP BY unqiueP.parentId
								) d3
			ON items.id = d3.parentId
            
            GROUP BY items.id
            ORDER BY items.order ASC, items.lastUpdate ASC";


if($getAll != null){
    $sql = "select items.*,t2.image,d4.iconName,d4.ext from items

    LEFT JOIN

    (select id,itemId, filename as image, prefer from media, mediaItemMap where id = mediaId and media.delete!= 1 and mediaItemMap.prefer = 1 order by lastUpdateTime DESC) t2

    on items.id = t2.itemId

    LEFT JOIN (select fileName AS iconName,fileExt as ext, m2.itemId from media inner join mediaItemMap m2
            on media.id = m2.mediaId && m2.isIcon = 1) d4
            ON items.id = d4.itemId

    where items.enable = 1
    
    GROUP BY items.id
    
    order by items.order ASC, items.lastUpdate ASC";

    if($light != null && $light == 1){
            $sql = "select items.id, dictionary.en as title_en, items.type from items, dictionary where items.titleId = dictionary.id
    order by items.lastUpdate DESC;
    ";
    }
}

    $st = $conn->prepare($sql);



if($parentId != null){
    $st->bindValue( ":parentId", $parentId, PDO::PARAM_INT );
}
if($itemId != null){
    $st->bindValue( ":id", $itemId, PDO::PARAM_STR );
}



$st->execute();
//$st->debugDumpParams();
$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {

    /*if($row['id']!=$exclusionKey || ($source!=null && $source== "cms")|| ($appVersion!=null && $appVerNumArr[0]>=4 &&
        $appVerNumArr[1]>=5)
    ) {
        $list[] = $row;
    }*/
    $list[] = $row;
}

//pprint_r($list);

/*$sql = "select * from movie order by id ASC";
$st = $conn->prepare( $sql );
$st->execute();
$movieList = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $movieList[] = $row;
    //echo json_encode($row);
}*/

$conn = null;





//echo  '{"'.'result'.'":'.json_encode($list).'}';
echo returnStatus(1 , 'good',$list);


function returnItemStatus( $status, $msg,  $data_array = array(),$movieData=array()){
    $out = array(
        'status' => $status,
        'msg' => $msg,
        'data' => $data_array,
        'movieData' => $movieData
    );

    return json_encode($out);
}
?>
