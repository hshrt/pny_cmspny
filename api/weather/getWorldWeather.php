<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$date = isset($_REQUEST['date'])?$_REQUEST['date']:'';

//filter by status
$status = isset($_REQUEST['status'])&& strlen($_REQUEST['status']) > 0?$_REQUEST['status']:null;

$room = isset($_REQUEST['room'])&& strlen($_REQUEST['room']) > 0?$_REQUEST['room']:null;


try {
    $conn = new PDO(DB_DSN_WEATHER, DB_USERNAME_WEATHER, DB_PASSWORD_WEATHER);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

$conn->exec("set names utf8");

$sql = "select name as city,temp,icon, mapping from worldweather ww left join worldtime wt on ww.country = wt.country
where wt.name!='Hong Kong' && wt.name!='yangon'";

$st = $conn->prepare ( $sql );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

foreach($list as &$item){
    //pprint_r($item["mapping"]);
    $sql = "SET time_zone = :timezone";

    $st = $conn->prepare ( $sql );
    $st->bindValue(":timezone", $item["mapping"], PDO::PARAM_STR);
    $st->execute();

    $sql = "Select NOW() as time";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $resultlist = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $resultlist[] = $row;
    }

    //pprint_r($resultlist);

    //pprint_r($item);

    //pprint_r('fuck');

    $item["time"] = $resultlist[0]["time"];

    //pprint_r($item);

}

//pprint_r($list);


if($st->fetchColumn() > 0 || $st->rowCount() > 0){

    echo returnStatus(1, 'get weather OK',$list);
}
else{
    echo returnStatus(0, 'get weather fail',$list);
}


return 0;

?>
