<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
$_SESSION['when'] = time();

$date = isset($_REQUEST['date'])?$_REQUEST['date']:'';

//filter by status
$status = isset($_REQUEST['status'])&& strlen($_REQUEST['status']) > 0?$_REQUEST['status']:null;

$room = isset($_REQUEST['room'])&& strlen($_REQUEST['room']) > 0?$_REQUEST['room']:null;

$device = isset($_REQUEST['device'])?$_REQUEST['device']:null;


if ( 0){
    echo returnStatus(0, 'missing room number');
}

else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $base = "select id,room,foodIdList, deliveryTime, orderTime, numOfGuest,quantity,status, lastUpdate, lastUpdateBy from orders";
    $sql = $base;

    if(isset($_REQUEST["date"])){
        $sql = $sql." where orderTime >:date1 && 
orderTime < DATE
(DATE_ADD(:date1, interval 1 
        day))". " && status != 9 ";
    }

    if(isset($_REQUEST["status"])&& strlen($_REQUEST['status']) > 0){
        $sql = $sql." && status = :status ";
    }
    if($room!=null){
        $sql = $sql." && room = :room ";
    }
    
    $orderBy = "order by orderTime DESC";
    
    $sql = $sql.$orderBy;

    //only for BSP
    if($device == "BSP"){
        $sql = $base." where  room = :room && status!=2 && status!=9 && deleted!=1 ".$orderBy;
    }
    
    $st = $conn->prepare ( $sql );

    if($device != "BSP") {
        $st->bindValue(":date1", $date, PDO::PARAM_STR);
    }
    if(isset($_REQUEST["status"])&& strlen($_REQUEST['status']) > 0) {
        $st->bindValue(":status", $status, PDO::PARAM_INT);
    }
    if($room!=null) {
        $st->bindValue(":room", $room, PDO::PARAM_STR); 
    }

    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }
    for($x=0;$x< sizeof($list);$x++){
        $foodIdList = explode(',', $list[$x]["foodIdList"]);
        //pprint_r($foodIdList);
        $quantitys[] = array();

        for($y=0; $y< sizeof($foodIdList); $y++){

            $itemId=0;


            if($y==0) {
                $itemId = $foodIdList[$y];
                $quantity = 0;
            }
            else {
                $parseArr = explode(':', $foodIdList[$y]);

                if(sizeof($parseArr)==0){
                    $itemId = $foodIdList[$y];
                }
                else{
                    $itemId = $parseArr[0];
                }
                if(sizeof($parseArr)>1)
                $quantity = $parseArr[1];
            }



            $sql = "select d.en from dictionary d inner join
                        items i on i.titleId=d.id && i.id=:foodId";

            //echo $sql;
            $st = $conn->prepare ( $sql );
            $st->bindValue( ":foodId",$itemId, PDO::PARAM_STR );

            $st->execute();

            $orderDetails = array();
            while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
                $orderDetails[] = $row;
            }

            //only if quantity >=2, we need to show quantity
            if($quantity<=1){
                $quantity="";
            }
            else{
                $quantity=" x ".$quantity;
            }
           /* pprint_r($orderDetails);
            pprint_r($itemId);
            pprint_r($quantity);*/

            if($y==0) { //for the root item

                if(sizeof($orderDetails)>0) {

                    $list[$x]["item"] = $orderDetails[0]["en"];
                }
            }
            else if ($y==1){ //for the choice items

                if(sizeof($orderDetails)>0) {

                    $list[$x]["choices"] = $orderDetails[0]["en"].$quantity;
                    /*pprint_r($orderDetails[0]["en"]);
                    pprint_r($list[$x]["choices"]);*/
                }
            }
            else{
                if(sizeof($orderDetails)>0) {

                    $list[$x]["choices"] = $list[$x]["choices"] . ", " . $orderDetails[0]["en"].$quantity;
                }
            }

            //unset($list[$x]["foodIdList"]);
        }
    }

    if($st->fetchColumn() > 0 || $st->rowCount() > 0){

        echo returnStatus(1, 'get Order OK',$list);
    }
    else{
        echo returnStatus(0, 'get Order fail',$list);
    }
}

return 0;

?>
