<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$itemId = isset($_POST['itemId'])?$_POST['itemId']:null;
$currency = isset($_POST['currency'])?$_POST['currency']:null;
$rate = isset($_POST['rate'])?$_POST['rate']:null;

if ( empty($currency)){
    echo returnStatus(0, 'missing currency ');
    exit;
}



$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if($itemId!=null){
    $sql = "UPDATE fx SET currency=:currency,rate=:rate,lastUpdate=now(),lastUpdateBy=:lastUpdateBy where id = :itemId";
}
else{
    $sql = "INSERT INTO fx (currency, rate,lastUpdate,
            lastUpdateBy) VALUES (:currency,:rate,now(),
            :lastUpdateBy)";
}

$st = $conn->prepare ( $sql );

if($itemId!= null){
    $st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );
}

$st->bindValue( ":currency", $currency, PDO::PARAM_STR );
$st->bindValue( ":rate", strval($rate), PDO::PARAM_STR );
$st->bindValue( ":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR );


$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'update ok!');
else
    echo returnStatus(0 , 'update fail! May be there is no change?');

$conn = null;

?>
