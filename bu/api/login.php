<?php

session_start();

//ini_set( "display_errors", true );
require( "../config.php" );
require("../php/inc.appvars.php");

$username = $_POST['email'];
$password = md5($_POST['pwd']);


// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT * FROM user WHERE email= :email AND pass=:password";

$st = $conn->prepare ( $sql );
$st->bindValue( ":email", $username, PDO::PARAM_STR );
$st->bindValue( ":password", $password   , PDO::PARAM_STR );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

$conn = null;


if(count($list)>=1){
    $_SESSION['email']=$list[0]['email'];
    $_SESSION['role']=$list[0]['role'];
    $_SESSION['userId']=$list[0]['id'];

    echo returnStatus(1 , 'Login succeed',array('email' => $list[0]['email']));
}
else{
    echo returnStatus(0 , 'Login failed');
}

?>

