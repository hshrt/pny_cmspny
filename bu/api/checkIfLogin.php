<?php

session_start();

ini_set( "display_errors", true );
require_once( "../config.php" );
require_once("../php/inc.appvars.php");

if(!isset($_SESSION['email']) || $_SESSION['email'] == null || sizeof($_SESSION['email']) == 0){
    echo returnStatus(0 , 'No Authentication');
    exit;
}
else{
    if(isset($_POST["ajaxLoop"]) && $_POST["ajaxLoop"] == true){
        echo returnStatus(1 , 'have Authentication');
    }
}

?>

