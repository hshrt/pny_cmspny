<?php

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();

$roomNum = isset($_POST['roomNum'])?$_POST['roomNum']:'';
$regNum = isset($_POST['regNum'])?$_POST['regNum']:'';
$oldRoomNum= isset($_POST['oldRoomNum'])?$_POST['oldRoomNum']:'';
$isOldRoomVacant= isset($_POST['isOldRoomVacant'])?$_POST['isOldRoomVacant']:'';
$isNewRoomVacant = isset($_POST['isNewRoomVacant'])?$_POST['isNewRoomVacant']:'';

$lastUpdateBy = isset($_POST['lastUpdateBy'])?$_POST['lastUpdateBy']:'';


if ( empty($roomNum)){
    echo returnStatus(0, 'missing room number');
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");


    $sql = "UPDATE guest SET room=:newRoom,lastUpdate=now(),lastUpdateBy=:lastUpdateBy where room = :oldRoom";
    $st = $conn->prepare ( $sql );
    $st->bindVAlue( ":newRoom", $roomNum, PDO::PARAM_STR);
    $st->bindValue( ":oldRoom", $oldRoomNum, PDO::PARAM_STR );
    $st->bindValue( ":lastUpdateBy",$lastUpdateBy , PDO::PARAM_STR );
    //$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
    $st->execute();


    if($st->rowCount() > 0){
        echo returnStatus(1, 'Move Room OK');
    }
    else{
        echo returnStatus(0, 'Move Room fail');
    }
}
return 0;

?>
