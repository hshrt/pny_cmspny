<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$id = isset($_REQUEST['id'])?$_REQUEST['id']:null;


if ( $id==null){
    echo returnStatus(0, 'missing item id');
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "UPDATE  items SET optionSetIds = :optionSetsList where id = :id";

$st = $conn->prepare ( $sql );

$st->bindValue( ":optionSetsList",$optionSetsList, PDO::PARAM_STR );
$st->bindValue( ":id",$id, PDO::PARAM_STR );


$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}


if($st->fetchColumn() > 0 || $st->rowCount() > 0){

    echo returnStatus(1, 'get list OK',$list);
}
else{
    echo returnStatus(0, 'get list fail',$list);
}


return 0;

?>
