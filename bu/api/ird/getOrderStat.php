<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/:year
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
$_SESSION['when'] = time();

$year = isset($_REQUEST['year'])?$_REQUEST['year']:null;
$month = isset($_REQUEST['month'])?$_REQUEST['month']:null;


if ($year==null){
    echo returnStatus(0, 'missing year');
    exit;
}
if ($month==null){
    echo returnStatus(0, 'missing month');
    exit;
}

else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $base = "select 
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month) As total, 
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month && o.status = 2) As cancelled,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month && o.status = 1) As processed,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.orderTime) >= 6 &&hour(o.orderTime)<12 && o.status = 1) As placed1,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.orderTime) >= 12 &&hour(o.orderTime)<18 && o.status = 1) As placed2,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.orderTime) >= 18 &&hour(o.orderTime)<=23 && o.status = 1) As placed3,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.orderTime) >= 0 &&hour(o.orderTime)<6 && o.status = 1) As placed4,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.deliveryTime) >= 6 &&hour(o.deliveryTime)<12 && o.status = 1) As deliver1,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.deliveryTime) >= 12 &&hour(o.deliveryTime)<18 && o.status = 1) As deliver2,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.deliveryTime) >= 18 &&hour(o.deliveryTime)<=23 && o.status = 1) As deliver3,
(select Count(*) from orders o where year(o.orderTime) = :year && month(o.orderTime)=:month &&hour(o.deliveryTime) >= 0 &&hour(o.deliveryTime)<6 && o.status = 1) As deliver4;";
    
    $sql = $base;

    
    $st = $conn->prepare ( $sql );



    $st->bindValue(":year", $year, PDO::PARAM_INT);
    $st->bindValue(":month", $month, PDO::PARAM_INT);


    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    if($st->fetchColumn() > 0 || $st->rowCount() > 0){

        echo returnStatus(1, 'get Order Stat OK',$list);
    }
    else{
        echo returnStatus(0, 'get Order Stat fail',$list);
    }
}

return 0;

?>
