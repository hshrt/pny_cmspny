<?php
/**
 * Created by PhpStorm.
 * User: esd
 * Date: 9/22/14
 * Time: 10:12 AM
 */
ini_set( "display_errors", true );

require( "../config.php" );
require("../php/inc.appvars.php");
require("../php/func_nx.php");
/*require( "/var/www/html/cms/config.php" );
require("/var/www/html/cms/php/inc.appvars.php");
require("/var/www/html/cms/php/func_nx.php");*/


//copy all the rows from BPC database
$conn = new PDO( DB_DSN_BPC, DB_USERNAME_BPC, DB_PASSWORD_BPC );
$conn->exec("set names utf8");

$sql = "select * from allroom";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

pprint_r($list);

$conn = null;

//insert into CMS database
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "TRUNCATE allroom;";
$st = $conn->prepare ( $sql );
$st->execute();

for($x=0;$x<sizeof($list);$x++){
    $item = $list[$x];
    $sql = "INSERT INTO allroom (room,type,chkin,indate,outdate,faxnum,guestname,lang) VALUES
    (:room,:type,:chkin,:indate,:outdate,:faxnum,:guestname,:lang)";
    $st = $conn->prepare ( $sql );

    $st->bindValue( ":room", $item['room'], PDO::PARAM_STR);
    $st->bindValue( ":type", $item['type'], PDO::PARAM_STR);
    $st->bindValue( ":chkin", $item['chkin'], PDO::PARAM_STR);
    $st->bindValue( ":indate", $item['indate'], PDO::PARAM_STR);
    $st->bindValue( ":outdate", $item['outdate'], PDO::PARAM_STR);
    $st->bindValue( ":faxnum", $item['faxnum'], PDO::PARAM_STR);
    $st->bindValue( ":guestname", $item['guestname'], PDO::PARAM_STR);
    $st->bindValue( ":lang", $item['lang'], PDO::PARAM_STR);
    $st->execute();
}

$conn = null;



?>