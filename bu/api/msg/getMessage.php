<?php 

ini_set( "display_errors", true );

require( "../../config.php" );
require( "../../php/func_nx.php");
require("../../php/inc.appvars.php");

if(isset($_POST['id'])){
    $id= $_POST["id"];
}

$getAll = $getActive =  $getInactive = false;

if(isset($_REQUEST['getAll'])){
    $getAll = $_REQUEST['getAll'];
}

$getActive = isset($_REQUEST['getActive'])?$_REQUEST['getActive']:null;

$getInactive = isset($_REQUEST['getInactive'])?$_REQUEST['getInactive']:null;

$roomId = isset($_REQUEST['roomId'])?$_REQUEST['roomId']:null;

$itemId = isset($_REQUEST['itemId'])?$_REQUEST['itemId']:null;

$lang = isset($_REQUEST['lang'])?$_REQUEST['lang']:"E";

$currentTime = isset($_REQUEST['currentTime'])?$_REQUEST['currentTime']:null;

//E":"0","F":"1","CT":"2","CS":"3","J":"4","G":"5","K":"6","R":"7","P":"8",
switch($lang){
    case 'E' : $lang = 'en';break;
    case 'F' : $lang = 'fr';break;
    case 'CT' : $lang = 'zh_hk';break;
    case 'CS' : $lang = 'zh_cn';break;
    case 'J' : $lang = 'jp';break;
    case 'G' : $lang = 'de';break;
    case 'K' : $lang = 'ko';break;
    case 'R' : $lang = 'ru';break;
    case 'P' : $lang = 'pt';break;
    case 'A' : $lang = 'ar';break;
    case 'S' : $lang = 'es';break;

    default:  $lang = 'en';break;

}

$page = 0;
$itemPerPage = 15;

if(isset($_POST['page'])){
    $page = $_POST['page'];
}
if(isset($_POST['itemPerPage'])){
    $itemPerPage = $_POST['itemPerPage'];
}

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$phase = "";
if($getActive) $phase = " && m.status != 'D' ";
if($getInactive) $phase = " && m.status = 'D' ";

$phase2="";
if($itemId) $phase2 = " where combineList_big.id='".$itemId."' ";


if($getAll) {
    $sql = "SELECT * from message order by startDate DESC";
}
else if($getActive ){

    $sql = "select combineList_big.id, combineList_big.name_list,combineList_big.room_list as room_list, d.en as subject,
combineList_big.subjectId,
    combineList_big.descriptionId,combineList_big.startDate,combineList_big.endDate,combineList_big.priority,combineList_big.boardcast,combineList_big.lastUpdateBy,
    combineList_big.source, combineList_big.lastUpdate from dictionary_msg d

    inner join

    (select m.id,combineList.name_list,combineList.room_list, m.subjectId,m.descriptionId, startDate,endDate,
    priority, boardcast,
    lastUpdateBy,lastUpdate,
    m.source from message m

    inner join

    (select distinct messageId, group_concat(d2.guestname order by d2.guestname) as name_list, group_concat(d2.room
    order by d2.room) as room_list
    from roomMessageMap d1
    left join allroom d2 on d1. room = d2 .room group by messageId order by messageId) combineList

    on m.id = combineList.messageId ".$phase.")combineList_big

    on combineList_big.subjectId = d.id ".$phase2."

    order by combineList_big.lastUpdate DESC";

    //echo($sql);

}
else if($getInactive){
    $sql = "select combineList_big.id, d.en as subject,combineList_big.subjectId,
    combineList_big.descriptionId,combineList_big.startDate,combineList_big.endDate,combineList_big.priority,combineList_big.boardcast,combineList_big.status,combineList_big.lastUpdateBy,
    combineList_big.source from dictionary_msg d

    inner join

    (select m.id, m.subjectId,m.descriptionId, startDate,endDate,priority, boardcast,status,
    lastUpdateBy,
    m.source from message m where status = 'D') combineList_big



    on combineList_big.subjectId = d.id ".$phase2."

    order by combineList_big.endDate DESC";
}
else if($itemId){
    $sql = "select combineList_big.id, d.en as subject,combineList_big.subjectId,
    combineList_big.descriptionId,combineList_big.startDate,combineList_big.endDate,combineList_big.priority,combineList_big.boardcast,combineList_big.status,combineList_big.lastUpdateBy,
    combineList_big.source from dictionary_msg d

    inner join

    (select m.id, m.subjectId,m.descriptionId, startDate,endDate,priority, boardcast,status,
    lastUpdateBy,
    m.source from message m) combineList_big



    on combineList_big.subjectId = d.id ".$phase2."

    order by combineList_big.endDate DESC";
}

//this is for android app to get message for specific room
if($roomId!=null){
    $sql = "select distinct m.*,
d1.".$lang." as subject,
d2.".$lang." as description
from message m
inner join roomMessageMap rmm ON (m.id = rmm.messageId || m.boardcast = 1) && m.startDate <= :currentDate && (m
.endDate
>= :currentDate || m.endDate = '0000-00-00 00:00:00')
inner join allroom g ON (g.room = rmm.room
    and g.room = :roomId and m.status !='D') or (m.boardcast = 1 and m.status !='D')
    inner join dictionary_msg d1 on d1.id = m.subjectId
    inner join dictionary_msg d2 on d2.id = m.descriptionId
    order by lastUpdate DESC";


}



$st = $conn->prepare ($sql);

if($roomId!=null){
    $st->bindVAlue( ":roomId", $roomId, PDO::PARAM_STR);
    $st->bindVAlue( ":currentDate", $currentTime, PDO::PARAM_STR);
}

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

$conn = null;

echo returnStatus(1 , 'good',$list);


?>
