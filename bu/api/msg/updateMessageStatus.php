<?php 

ini_set( "display_errors", true );

require( "../../config.php" );
require( "../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$msgId = isset($_REQUEST['msgId'])?$_REQUEST['msgId']:null;
$room = isset($_REQUEST['room'])?$_REQUEST['room']:null;



//setup DB
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "Update roomMessageMap rmm set rmm.read=1 where messageId = :id and room = :room";

$st = $conn->prepare ($sql);

$st->bindValue( ":id", $msgId, PDO::PARAM_STR );
$st->bindValue( ":room", $room, PDO::PARAM_STR );

$st->execute();


if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'update message good');
}
else{
    echo returnStatus(1 , 'update message fail');
}

$conn = null;

?>
