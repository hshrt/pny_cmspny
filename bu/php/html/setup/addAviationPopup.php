<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <div id="head">
            <h1 id="create_box_title">Aviation</h1>
            <div class='closeBtn' id='closeBtn'></div>
        </div>

        <div id="middle">


            <h4 id="field1_label">Name</h4>
            <input id="name" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>

            <h4 id="field2_label">IATA code</h4>
            <input id="iata" class='form-control' type='text' name='title' data-type='text' value='' autofocus=''>


        </div>

        <div id="foot">
            <div class="actions">
                <div style='float:left' class='submit round_btn btn bg-maroon' id="deleteBtn">Delete</div>
                <div style='float:right' class='submit round_btn btn bg-olive' id="saveCreateBtn">Save</div>
            </div>
        </div>
    </div>
</div>