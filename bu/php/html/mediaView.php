<div id="mediaViewContainer">
    <div class="box" id="media_boxer">
        <div class="box-header" style="padding-top:0px">
            <h3 style="float:left" id="media_head_title">Media</h3>
            <div class='submit round_btn btn bg-olive' id='addBtn'>+Add Media</div>
        </div>
        <div class="box-body">

            <div id="photoStreamContainer">

            </div>

            <div id='paginationContainer'>

            </div>

        </div><!-- /.box-body -->
        <div class="box-footer">

        </div><!-- /.box-footer-->
    </div>
</div>
