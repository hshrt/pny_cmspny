<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box'>
        <h1 id="create_box_title"></h1>
        <div class='closeBtn' id='closeBtn'></div>
        <div class="input single" id="titleContainer">

            <label for="title">Title</label>
            <input id="title" class='form-control' type="text" name="title" tabindex="1" data-type="text" value="" autofocus="" maxlength="150">
            <p class="inlineError"></p>
        </div>
        <div class="input single">
            <label id="typeLabel" for="title" style='width:100%'>Type</label>
            <select class='form-control' id='selectionBox' name='type'>
                <option value="Information">Information</option>
                <!--<option value="Information with top sub menu">Information (with top sub menu)</option>-->
                <option value="Information with sub menu">Information (with sub menu)</option>
                <option value="Information with top sub menu">Information (with top sub menu)</option>
                <option value="Guest request">Guest request</option>
                <option value="Spa/Restaurant">Spa/Restaurant</option>
                <option value="OptionSets">OptionSets</option/>
                <option value="Web">Web</option/>
            </select>
            <p class="inlineError"></p>
        </div>
        <div class="actions">
            <div style='float:right' class='btn btn-lg bg-olive' id="createBtn">Create</div>
        </div>
    </div>
</div>