App.SubTopBar = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    tab:"",
    highlightColor:"#fff",
    dimColor:"#453E33",
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        this.self = this;
        if(options && options.isEdit){
            this.isEdit = options.isEdit;
        }

        this.render();
    },
    events: {

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        console.log("signInView render");
        var self = this;
        console.log("render SignInView");
        $.ajax({
         url : "php/html/subTopBar.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){
                    //$('.popup_box_container').show(true);

                    self.hightlightTab(self.tab);

                    //only administrator can do admin work
                    if(App.userRole != "admin"){
                        $("#adminTab, #adminLogo").remove();
                    }





                    $("#contentTab").attr("href","#/property/"+App.hotelLocation+"/content/library");
                    $("#setupTab").attr("href","#/property/"+App.hotelLocation+"/setup/airline");
                    $("#adminTab").attr("href","#/property/"+App.hotelLocation+"/user");
                    $("#guestTab").attr("href","#/property/"+App.hotelLocation+"/guests");
                    $("#messagingTab").attr("href","#/property/"+App.hotelLocation+"/messages/active");


                    /*if(App.hotelLocation == "hongkong"){

                        $('#guestTab').removeAttr('href')
                        $('#messagingTab').removeAttr('href')
                    }*/
                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });


    },

    hightlightTab: function(tab){
        this.tab = tab;


        this.dimAllTab();

        if(tab == 'guests'){
            $("#guestTab").css({color:this.highlightColor});
            $("#guestLogo").css({color:this.highlightColor});
        }
        else if(tab == 'messages'){
            $("#messagingTab").css({color:this.highlightColor});
            $("#msgLogo").css({color:this.highlightColor});
        }
        else if(tab == 'content'){
            $("#contentTab").css({color:this.highlightColor});
            $("#contentLogo").css({color:this.highlightColor});
        }
        else if(tab == 'setup'){
            $("#setupTab").css({color:this.highlightColor});
            $("#settingLogo").css({color:this.highlightColor});
        }
        else if(tab == 'user'){
            $("#adminTab").css({color:this.highlightColor});
            $("#adminLogo").css({color:this.highlightColor});
        }
    },

    dimAllTab: function(){
        $("#guestTab").css({color:this.dimColor});
        $("#messagingTab").css({color:this.dimColor});
        $("#contentTab").css({color:this.dimColor});
        $("#setupTab").css({color:this.dimColor});
        $("#adminTab").css({color:this.dimColor});

        $("#guestLogo").css({color:this.dimColor});
        $("#msgLogo").css({color:this.dimColor});
        $("#contentLogo").css({color:this.dimColor});
        $("#settingLogo").css({color:this.dimColor});
        $("#adminLogo").css({color:this.dimColor});
    },

    close :function(){
        console.log("close fire");
    },
    getSelf: function(){
        return this.self;
    },
    destroy: function() {
        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        App.imageData = null;
        App.imageFileName = null;
        $(".popup_box_container").remove();
        this.undelegateEvents();

        $(document).unbind('scroll');
        $('body').css({'overflow':'visible'});

    },
    isHide : false
});