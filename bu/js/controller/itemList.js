App.ItemList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0, //0: Library 1:Dining 2:Concierge
    jsonObj: null,
    disbleMain:false,
    title: "Library",
    type: "",
    lastClickObject:null,
    initialize: function(options){
        if(options && options.parentId){
            this.parentId = options.parentId;
        }
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        if(options && options.type){
            this.type = "_"+options.type;
            //alert(this.title);
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },

    render: function(){

        var self = this;
        $.ajax({
            url : "php/html/itemList.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){

            console.log(html);

            $(self.el).append(html).promise()
                .done(function() {
                    //alert("done");

                    if(self.type!=null) {
                        $("table").last().attr("id", "resultTable" + self.type);
                    }

                    self.postUISetup();
                    $("#boxer").after($("#itemListContainer"));

                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },

    postUISetup:function(){

        //we don't have search for itemList component
        $("#search").hide();

        //Insert Table Header
        var tableHeaderString = "<th class='tableHead'>Title</th>"+
            "<th class='tableHead'>Type</th>"+
            "<th class='tableHead'></th>";

        if(this.title != "Library"){
            tableHeaderString = "<th style='width:30%'>Title</th>"+
            "<th style='width:55%'>Description</th>"+
            "<th class='tableHead'></th>";;
        }

        $("#resultTable"+this.type).append(
            "<tbody><tr>"+
            tableHeaderString+
            "</tr><tbody>");



        /*$(this.el).append(
            "<div id='itemListContainer'>"+
            "<div class='table-header'>"+
            "<h1>Library</h1>"+
            "<div class='submit round_btn' id='addBtn2'>+Add Page</div>"+
            "</div>"+
            "<table id='headerTable' class = 'table table-striped'>"+
            "<tr class='headRow2'>"+
            tableHeaderString+
            "</tr></table>"+

            "<table id='resultTable2' class = 'table table-striped table-bordered'>"+
            "<tr class='headRow'>"+
            tableHeaderString+
            "</tr></table>"+

            "</table></div>"

        );*/

        var that = this;
        var self = this;

        $("#item_list_head_title").text(this.title);

        $('#addBtn').on('click',function(){
            //alert("delete item id = " + App.currentId);
            that.showPopup();
        });

        //setup add btn text
        var addBtnText = that.title == "Library"?"Page":that.title;
        $('#addBtn').text("+Add " + addBtnText);

        App.showLoading();
        $.ajax({
            url : "api/getItemList.php",
            method : "POST",
            dataType: "json",
            data : {parentId:that.parentId }
        }).success(function(json){
            //setTimeout(App.hideLoading, 1000);

            App.hideLoading();

            console.log(json);
            App.libList.jsonObj = json.data;

            for(var y = 0; y<json.data.length;y++){

                var secondColContent = "<td>"+json.data[y].type+"</td>";

                if(that.title != "Library"){
                    secondColContent = "<td >"+"<div id='"+ "desText"+y+"' class='descriptionText'>"+json.data[y].description_en+"</div>" + "</td></div>";
                }

                //$(".descriptionText").dotdotdot({ellipsis	: '... ',height: "70px",wrap: 'word'});

                if(json.data[y].child_names == json.data[y].title_en || that.title != "Library"){
                    json.data[y].child_names = "";
                }
                //id='"+json.data[y].id+
                var thirdColContent = "<td><div>"+json.data[y].child_names+"</div>"+"<div id='"+json.data[y].id+"enable"+"' class='enableBtn'>on</div>"+"</td>";
                //var fourthColContent = "<td><div>"+"tester"+"</div></td>";

                //if is not library, we will replace the child names with the swap order button
                if(that.title != "Library" || true){
                    thirdColContent = "<td><div class='swapBtn' title='drag it to swap the order.'></div>"+"<div id='"+json.data[y].id+"enable"+"' class='enableBtn'>on</div>"+"</td>";


                }
                else{
                    thirdColContent = "<td><div style='float:left; position:relative;'><div style='float:left; position:relative;'>"+json.data[y].child_names+"</div></div>"+"<div id='"+json.data[y].id+"enable"+"' class='enableBtn'>on</div>"+"</td>";

                    if(json.data[y].type == "Information"){
                        thirdColContent = "<td>"+"<div id='"+json.data[y].id+"enable"+"' class='enableBtn'>on</div>"+"</td>";
                    }
                }

                $("#resultTable"+that.type).append(
                    "<tr id='"+json.data[y].id+"' style='height:30px'><td>"+"<div class ='titleContainer'></div><div id='img"+y +"' class='smallThumb'> </div>" +"<div class='titleText'>"+json.data[y].title_en+"</div></div>"+"</td>"
                    +secondColContent+
                    thirdColContent+ "</tr>");

                if(!json.data[y].fileName || self.isRootPage() || json.data[y].ext == "p"){

                    $("#img"+y).hide();
                    $(".titleText").css({margin: "0px"});
                }
                else{
                    console.log("no file name at " + json.data[y] + " number = " + y);
                    console.log($("#img"+y));
                    $("#img"+y).css({
                        "background-image": 'url('+"upload/"+json.data[y].fileName+"_s.jpg"+')'
                    });
                }

                if(json.data[y].enable == 0){
                    console.log("checked disnable");
                    console.log()
                    $("#"+json.data[y].id+"enable").addClass("disable");
                    $("#"+json.data[y].id+"enable").text("off");
                }
                else{
                    console.log()
                    console.log("checked enable");
                }

                $("#"+json.data[y].id).attr( "index", y );

                (function (jsonitem) {
                    //alert(Area);
                    $("#"+jsonitem.id).on('click',function(){
                        //alert($(this).attr('index'));
                        //alert(document.URL);

                        if(!self.disbleMain){
                            App.currentId = $(this).attr('id');
                            App.currentItem = jsonitem;
                            //alert(jsonObj.result[y].id);
                            window.location = document.URL+"/"+$(this).attr('id');
                        }
                    });

                    $("#"+jsonitem.id+"enable").on('click',function(){
                        //alert($(this).attr('index'));
                        //alert(document.URL);
                        //App.currentId = $(this).attr('id');
                        //alert($(this).attr('id'));
                        self.lastClickObject = $(this);

                        $.ajax({
                            url : "api/enableItem.php",
                            method : "POST",
                            dataType: "json",
                            data : {itemId:$(this).attr('id').substr(0,$(this).attr('id').length-6)}
                        }).success(function(json){

                            if(json.status == 502){
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }

                            console.log("return data = "+ json.data);

                            if(json.data == 1){
                                //alert('yeah1');
                                //alert($(this));
                                $(self.lastClickObject).removeClass("disable");
                                $(self.lastClickObject).addClass("enableBtn");
                                $(self.lastClickObject).text("on");
                            }
                            else if(json.data == 0){
                                //alert('yeah2');
                                //alert($(self.lastClickObject).attr('id'));
                                $(self.lastClickObject).addClass("disable");
                                $(self.lastClickObject).text("off");
                            }

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                        //App.currentItem = jsonitem;
                        //alert(jsonObj.result[y].id);
                        //window.location = document.URL+"/"+$(this).attr('id');



                    });
                    $("#"+jsonitem.id+"enable").on('mouseover',function(){
                        //alert($(this).attr('index'));

                        self.disbleMain = true;
                        console.log("Mouseover");

                    });
                    $("#"+jsonitem.id+"enable").on('mouseout',function(){
                        //alert($(this).attr('index'));

                        self.disbleMain = false;
                        console.log("mouseout");

                    });

                })(json.data[y]);
            }

            //ellipsis function for the description text
            $(".descriptionText").dotdotdot({ellipsis	: '... ',height: "60px",wrap: 'word',watch: "window"});

            //enable the show tip box function for swap button (a request by Chris)
            $( ".swapBtn" ).tooltip();

            console.log(App.idArray);
            console.log(App.typeArray);



            /*setup swapping function*/
            var fixHelper = function (e, tr) {
                var $originals = tr.children();
                var $helper = tr.clone();
                $helper.children().each(function (index) {
                    // Set helper cell sizes to match the original sizes
                    $(this).width($originals.eq(index).width());
                });

                // append it to the body to avoid offset dragging
                $("#container").append($helper);

                return $helper;
            }

            var stopHelper = function(e, ui) {
                //alert("stop sorting");
                //alert( "Index: " + $("tr").index( $( "#140" )));
                console.log("total = " + App.libList.jsonObj.length);

                var idArr = new Array();
                var orderArr = new Array();


                $.each( App.libList.jsonObj, function( index, obj ){
                    console.log("obj.id = " + obj.id);

                    obj.order = $("tr").index($( "#"+obj.id));

                    console.log("obj.order = " + obj.order);

                    idArr.push(obj.id);
                    orderArr.push(obj.order-1);
                });


                App.showLoading();

                $.ajax({
                    url : "api/saveOrderForItem.php",
                    method : "POST",
                    dataType: "json",
                    data : {idArr:idArr.join(), orderArr:JSON.stringify(orderArr)}
                }).success(function(json){
                    console.log(json);
                    if(json.status == 502){
                        alert(App.strings.sessionTimeOut);
                        location.reload();
                        return;
                    }

                    App.hideLoading();

                    if(json.status == 1){
                        console.log("The order is saved.");
                        App.showTipBox("suscess-mode","Suscess","The order is saved.");
                    }

                }).error(function(d){
                    console.log('error');
                    console.log(d);
                    App.hideLoading();
                    App.showTipBox("fail-mode","Failed","Please try again later");

                });

                return ui;
            }

            $('#resultTable  tbody').sortable({
                helper: fixHelper,
                axis: 'y',
                stop: stopHelper
            }).disableSelection();

            /*$('.headRow').sortable({
                helper: fixHelper,
                axis: 'y'
            }).disableSelection();*/

           // $('.headRow').sortable( "disable" );
            $('#resultTable  tbody').sortable( "disable" );

            $('.swapBtn').on('mouseover',function(){
                $('#resultTable tbody').sortable( "enable" );
            });
            $('.swapBtn').on('mouseout',function(){
                $('#resultTable tbody').sortable( "disable" );
            });

            //if there is not more than one row, we dont need swap Button
            if(json.data.length <= 1){
                $('.swapBtn').hide();
            }

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });
    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library"?"Page":self.title,
                parentId: self.parentId
            }
        );
    },
    isRootPage: function(){
        var self = this;
        console.log("this.parentId = " + self.parentId);
        if(this.parentId == 0   || this.parentId == 2){

            return true;
        }
        else{
            return false;
        }
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        $("#item_list_boxer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});