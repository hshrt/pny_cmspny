<!-- popup box setup -->
<div id="subTopBarContainer">
    <div id="ulContainer">
    <ul>
        <li class=""><span class="glyphicon glyphicon-user" id="guestLogo"></span><a id="guestTab" href="#/property/beverly-hill/guests">Guests</a>
        </li>
        <li class=""><span class="glyphicon glyphicon-envelope" id="msgLogo"></span><a id="messagingTab"
                                                   href="#/property/beverly-hill/messages/active">Messaging</a>
        </li>
        <li class="current"><span class="glyphicon glyphicon-book" id="contentLogo"></span><a id="contentTab"
                                                        href="#/property/beverly-hill/content/library">Content</a>
            <!-- <li data-icon="metrics"><a href='#/property/hong-kong/metrics'>Metrics</a> -->
        </li>
        <li ><span class="glyphicon glyphicon-cog" id="settingLogo"></span><a id="setupTab" href="#/property/beverly-hill/setup/airline">Setup</a>
        </li>
        <li ><!--<span class="glyphicon glyphicon-user" id="adminLogo"></span>-->
            <i class="fa fa-fw fa-user" id="adminLogo" style="font-size: 15px;
  margin-right: -8px;"></i>
            <a id="adminTab" href="#/property/beverly-hill/user">User</a>
        </li></ul>
    </div>
</div>